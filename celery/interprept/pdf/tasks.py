
from interprept.celery.celery import celery
from interprept.services.pdfparser import convert_pdf_without_analizing, convert_pdf_to_text_and_save, convert_pdf_to_text, get_doc_info as get_info


@celery.task(time_limit=3600)
def convert_pdf_wo_analizing(file_path, output_path):
    return convert_pdf_without_analizing(file_path, output_path)

@celery.task(time_limit=1800)
def convert_pdf_and_save(file_path, output_path):
    return convert_pdf_to_text_and_save(file_path, output_path)

@celery.task(time_limit=1800)
def get_doc_info(file_path):
    return get_info(file_path)


@celery.task(time_limit=1800)
def convert_pdf(file_path):
    return convert_pdf_to_text(file_path)
