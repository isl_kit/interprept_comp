
from interprept.celery.celery import celery
from interprept.services.libwrapper import create_ne_list as cnl


@celery.task(time_limit=3600)
def create_ne_list(text, tag):
    return cnl(text, tag)
