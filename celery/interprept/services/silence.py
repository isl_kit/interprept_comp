#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

class Silence:

    def __init__(self, on=True):
        self.active = False
        self.on = on

    def open(self):
        if self.active or not self.on:
            return

        self.null_fds = [os.open(os.devnull, os.O_RDWR) for x in xrange(2)]
        self.save = os.dup(1), os.dup(2)
        os.dup2(self.null_fds[0], 1)
        os.dup2(self.null_fds[1], 2)
        self.active = True

    def close(self):
        if not self.active:
            return

        os.dup2(self.save[0], 1)
        os.dup2(self.save[1], 2)
        os.close(self.null_fds[0])
        os.close(self.null_fds[1])
        self.active = False
