#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import traceback
import re
from sys import exc_info
from codecs import open as codecs_open
from cStringIO import StringIO
from datetime import datetime

from guess_language import guess_language

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage


password = ""
maxpages = 0
codec = 'utf-8'
caching = True
pagenos = set()
laparams = LAParams(detect_vertical=True)
settings = None

def get_versions():
    with open(settings.VERSION_FILE, "r") as f:
        data = json.load(f)
    return data

def init_pdf(conf):
    global settings
    settings = conf

def find_date(text):
    m = re.search("(^|\s)([0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4})($|\s)", text)
    if m:
        try:
            return datetime.strptime(m.group(2), "%d.%m.%Y")
        except ValueError:
            return None
    return None

def find_a7_no(text):
    m = re.search("(^|\s)(A[0-9]-[0-9]{4}/[0-9]{4})($|\s)", text)
    if m:
        return m.group(2)
    return None

def find_pe_no(text):
    m = re.search("PE[0-9.]{4,9}v[0-9]{2}-[0-9]{2}", text)
    if m:
        return m.group(0)
    return None

def find_fdr_no(text):
    m = re.search("([A-Z]{2})?(\\\\[0-9]{1,6})?\\\\([0-9]{6,7})([A-Z]{2})", text)
    if m:
        return {
            "doc_type": m.group(1),
            "fdr_no": m.group(3),
            "tag": m.group(4).lower()
        }
    return None

def clean_text(text, language_tag):
    if language_tag == "ro":
        return text.replace(u"\u00DB", u"\u0219").replace(u"\u0015", u"\u021B")
    return text.replace(u"\u0015", u"").replace(u"\u0002", u"")


def isProperText(txt):
    if len(txt) < 3:
        return False
    nrOfAlphaNum = 0
    nrOfNonAlphaNum = 0
    for chr in txt:
        if chr.isalpha() is True:
            nrOfAlphaNum += 1
        else:
            nrOfNonAlphaNum += 1
    if nrOfAlphaNum > nrOfNonAlphaNum:
        return True
    return False


def convert_pdf_to_text_and_save(filepath, outputfile):
    result = convert_pdf_to_text(filepath)
    if result["text"] is not None:
        with codecs_open(outputfile, "wb+", "utf-8") as openfile:
            openfile.write(result["text"])

    return {
        "converted": result["text"] is not None,
        "tag": result["tag"],
        "fdr_no": result["fdr_no"],
        "doc_type": result["doc_type"],
        "pe_no": result["pe_no"],
        "a7_no": result["a7_no"],
        "published": result["published"],
        "version": result["version"],
        "error": result["error"]
    }

def get_doc_info(filepath):
    rsrcmgr = PDFResourceManager(caching=caching)
    error_occured = False
    error = ""
    fp = file(filepath, "rb")
    pe_no = None
    fdr_no = None
    tag = None
    doc_type = None
    published = None
    a7_no = None
    try:
        for idx, page in enumerate(PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching, check_extractable=True)):
            if idx > 9:
                break
            retstr = StringIO()
            device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams, imagewriter=None)
            try:
                interpreter = PDFPageInterpreter(rsrcmgr, device)
                interpreter.process_page(page)
                retstr.seek(0)
                raw_lines = [unicode(line.strip(), codec) for line in retstr.readlines()]
                if idx == 0:
                    date_state = 0
                    for raw_line in raw_lines:
                        tempdate = find_date(raw_line)
                        if tempdate is not None:
                            if date_state == 0:
                                date_state = 1
                                published = tempdate
                            elif date_state == 1 and published != tempdate:
                                date_state = 2
                                published = None
                        if a7_no is None:
                            a7_no = find_a7_no(raw_line)

                for raw_line in raw_lines:
                    if pe_no is None:
                        pe_no = find_pe_no(raw_line)
                    if fdr_no is None:
                        fdr_res = find_fdr_no(raw_line)
                        if fdr_res:
                            fdr_no = fdr_res["fdr_no"]
                            tag = fdr_res["tag"]
                            doc_type = fdr_res["doc_type"]
                    if pe_no and fdr_no:
                        break
                device.close()
            except:
                device.close()
                raise

    except:
        print traceback.format_exc()
        error_occured = True
        error = traceback.format_exc()
    fp.close()
    print error
    if error_occured is False:
        return {
            "tag": tag,
            "fdr_no": fdr_no,
            "doc_type": doc_type,
            "pe_no": pe_no,
            "a7_no": a7_no,
            "published": published.isoformat() if published else None,
            "error": error
        }
    else:
        return {
            "tag": tag,
            "fdr_no": None,
            "doc_type": None,
            "pe_no": None,
            "a7_no": None,
            "published": None,
            "error": error
        }


def convert_pdf_without_analizing(filepath, outputfile):
    rsrcmgr = PDFResourceManager(caching=caching)
    error_occured = False
    error = ""
    pages = []
    fp = file(filepath, "rb")
    try:
        for idx, page in enumerate(PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching, check_extractable=True)):
            retstr = StringIO()
            device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams, imagewriter=None)
            try:
                interpreter = PDFPageInterpreter(rsrcmgr, device)
                interpreter.process_page(page)
                retstr.seek(0)

                isPrevEmptyLine = True
                raw_lines = [unicode(line.strip(), codec) for line in retstr.readlines()]
                paragraphs = []
                lines = []
                for raw_line in raw_lines:
                    if isProperText(raw_line):
                        lines.append(raw_line)
                        isPrevEmptyLine = False
                    if not raw_line and not isPrevEmptyLine:
                        paragraphs.append(" ".join(lines))
                        lines = []
                        isPrevEmptyLine = True
                pages.append("\n\n".join(paragraphs))

                device.close()
            except:
                device.close()
                raise
    except:
        print traceback.format_exc()
        error_occured = True
        error = traceback.format_exc()
    fp.close()

    if error_occured is False:
        text = "\n\n\n".join(pages)
        tag = guess_language(text)
        text = clean_text(text, tag)
        with codecs_open(outputfile, "wb+", "utf-8") as openfile:
            openfile.write(text)
        return {
            "converted": True,
            "version": get_versions()["PDF"],
            "error": error
        }
    else:
        return {
            "converted": False,
            "version": get_versions()["PDF"],
            "error": error
        }


def convert_pdf_to_text(filepath):
    rsrcmgr = PDFResourceManager(caching=caching)
    error_occured = False
    error = ""
    pages = []
    fp = file(filepath, "rb")
    pe_no = None
    fdr_no = None
    tag = None
    doc_type = None
    published = None
    a7_no = None
    try:
        for idx, page in enumerate(PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching, check_extractable=True)):
            retstr = StringIO()
            device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams, imagewriter=None)
            try:
                interpreter = PDFPageInterpreter(rsrcmgr, device)
                interpreter.process_page(page)
                retstr.seek(0)

                isPrevEmptyLine = True
                raw_lines = [unicode(line.strip(), codec) for line in retstr.readlines()]
                paragraphs = []
                lines = []

                if idx == 0:
                    date_state = 0
                    for raw_line in raw_lines:
                        tempdate = find_date(raw_line)
                        if tempdate is not None:
                            if date_state == 0:
                                date_state = 1
                                published = tempdate
                            elif date_state == 1 and published != tempdate:
                                date_state = 2
                                published = None
                        if a7_no is None:
                            a7_no = find_a7_no(raw_line)

                for raw_line in raw_lines:
                    if idx < 10:
                        if pe_no is None:
                            pe_no = find_pe_no(raw_line)
                        if fdr_no is None:
                            fdr_res = find_fdr_no(raw_line)
                            if fdr_res:
                                fdr_no = fdr_res["fdr_no"]
                                tag = fdr_res["tag"]
                                doc_type = fdr_res["doc_type"]
                    if isProperText(raw_line):
                        lines.append(raw_line)
                        isPrevEmptyLine = False
                    if not raw_line and not isPrevEmptyLine:
                        paragraphs.append(" ".join(lines))
                        lines = []
                        isPrevEmptyLine = True
                pages.append("\n\n".join(paragraphs))


                device.close()
            except:
                device.close()
                raise
    except:

        print traceback.format_exc()
        error_occured = True
        error = traceback.format_exc()
    fp.close()
    if error_occured is False:
        text = "\n\n\n".join(pages)
        if tag is None:
            tag = guess_language(text)
        text = clean_text(text, tag)
        return {
            "text": text,
            "tag": tag,
            "fdr_no": fdr_no,
            "doc_type": doc_type,
            "pe_no": pe_no,
            "a7_no": a7_no,
            "published": published.isoformat() if published else None,
            "version": get_versions()["PDF"],
            "error": error
        }
    else:
        return {
            "text": None,
            "tag": None,
            "fdr_no": None,
            "doc_type": None,
            "pe_no": None,
            "a7_no": None,
            "published": None,
            "version": get_versions()["PDF"],
            "error": error
        }
