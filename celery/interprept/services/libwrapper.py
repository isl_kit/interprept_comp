#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from sys import path as sys_path
from os.path import abspath, exists as path_exists, join as join_path
from os import makedirs, getpid
import json
import traceback
import thread

import codecs

from .silence import Silence

settings = None
clients = {}
done = False

#supported_langs = {
#    "en": ["te", "ne"],
#    "de": ["te"]
#}
supported_langs = {
    "en": ["te", "ne"]
}

def init_system(conf):
    global settings
    global done

    if done is True:
        return

    settings = conf
    sys_path.append(settings.TERMNEXT_LIB_PATH)
    sys_path.append(settings.CONNECTOR_LIB_PATH)
    sys_path.append(settings.JAVA_LIB_PATH)
    sys_path.append("/usr/local/lib")

    import termnext_system_module
    import termnext_client_module
    termnext_system = termnext_system_module.Termnext_System()
    termnext_system.load_config(settings.CONFIG_PATH)
    termnext_system.load_posmodel(settings.POS_MODEL_PATH)

    for lang, _ in supported_langs.iteritems():
        client = termnext_client_module.Termnext_Client()
        client.set_system_ref(termnext_system, lang)
        client.init_nemodule(settings.NER_PATH, settings.NER_MODEL_PATH)
        clients[lang] = client

    done = True


def get_versions():
    with open(settings.VERSION_FILE, "r") as f:
        data = json.load(f)
    return data


def create_ne_list(text, tag):
    s = Silence(settings.DEBUG)
    if tag in supported_langs and "ne" in supported_langs[tag]:
        client = clients[tag]
    else:
        return {
            "nes": None,
            "version": "0.0.0",
            "error": "language not supported"
        }
    try:
        result = None
        try:

            s.open()
            result = client.ne_receiventities4datachunk(text.encode("utf-8"))
            s.close()

            entitiesintext = json.loads(result)
        except:
            s.close()
            print "EXCEPT 1"
            return {
                "nes": None,
                "version": "0.0.0",
                "error": traceback.format_exc()
            }

        entities = entitiesintext["tagged_entities"]
        print "FINISH NE"
        return {
            "nes": entities,
            "version": get_versions()["NE"],
            "error": ""
        }
    except:
        s.close()
        print "EXCEPT 2"
        print traceback.format_exc()
        return {
            "nes": None,
            "version": "0.0.0",
            "error": traceback.format_exc()
        }


def create_terminology_list(text, id, tag):
    s = Silence(settings.DEBUG)
    if tag in supported_langs and "te" in supported_langs[tag]:
        client = clients[tag]
    else:
        return {
            "sequences": None,
            "version": "0.0.0",
            "error": "language not supported"
        }
    try:
        s.open()
        client.push_sod(1)
        client.push_data(text.encode("utf-8"), id, tag.encode("utf-8"))
        total_docnr = 1
        client.push_eod(total_docnr)
        terminologylist = client.terminologylist(1, 1, -1, tag.encode("utf-8"))
        s.close()
        try:
            terminologylist = json.loads(terminologylist)
        except:
            print "EXCEPT 1"
            return {
                "sequences": None,
                "version": "0.0.0",
                "error": traceback.format_exc()
            }

        print "FINISH TE"
        return {
            "sequences": terminologylist["terminology_list"][:500],
            "version": get_versions()["TE"],
            "error": ""
        }
    except:
        s.close()
        print "EXCEPT 2"
        print traceback.format_exc()
        return {
            "sequences": None,
            "version": "0.0.0",
            "error": traceback.format_exc()
        }


def get_all_nes():
    ne_nypes = [
        {"id": 1, "name": "loc", "label": "location", "color": "#7EB127"},
        {"id": 2, "name": "org", "label": "organization", "color": "#306925"},
        {"id": 3, "name": "pers", "label": "person", "color": "#f8d214"},


        {"id": 8, "name": "number", "label": "number", "color": "#1f588d"},
        {"id": 5, "name": "amount", "label": "amount", "color": "#357fdf"},
        {"id": 7, "name": "perc", "label": "percent", "color": "#95bde8"},

        {"id": 4, "name": "date", "label": "date", "color": "#ff2eb4"},
        {"id": 6, "name": "time", "label": "time", "color": "#F20056"},


        {"id": 9, "name": "abbr", "label": "acronym", "color": "#e67839"},
        {"id": 10, "name": "article", "label": "article reference", "color": "#dadada"},
        {"id": 11, "name": "rule", "label": "rule reference", "color": "#adadad"},
        {"id": 12, "name": "resolution", "label": "resolution reference", "color": "#838383"},
        {"id": 13, "name": "directive", "label": "directive reference", "color": "#5e5e5e"}
    ]
    return {
        "nes_def": ne_types,
        "version": get_versions()["NE"]
    }
