from __future__ import absolute_import

from celery import Celery
from celery.signals import task_prerun
from interprept.services.libwrapper import init_system
from interprept.services.pdfparser import init_pdf

# instantiate Celery object
celery = Celery(include=[
    'interprept.ne.tasks',
    'interprept.terminology.tasks',
    'interprept.pdf.tasks'
])

# import celery config file
celery.config_from_object('celeryconfig')

@task_prerun.connect
def init(sender=None, conf=None, task=None, **kwargs):
    print celery.conf.CELERY_TASK_SERIALIZER
    if task.name.startswith("interprept.pdf."):
        init_pdf(celery.conf)
    else:
        init_system(celery.conf)

if __name__ == '__main__':
    celery.start()


