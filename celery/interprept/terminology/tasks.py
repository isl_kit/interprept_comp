from interprept.celery.celery import celery
from interprept.services.libwrapper import create_terminology_list as ctl


@celery.task(time_limit=3600)
def create_terminology_list(text, id, tag):
    return ctl(text, id, tag)
