from os import getenv
from os.path import abspath, basename, dirname, join, normpath
from sys import path
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

CELERY_ROOT = dirname(abspath(__file__))

CELERY_RESULT_SERIALIZER = 'json'
CELERY_TASK_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json']
BROKER_URL = getenv('BROKER_URL', 'amqp://')
CELERY_RESULT_BACKEND = 'amqp://'
CELERY_TIMEZONE = 'Europe/Berlin'
CELERY_ENABLE_UTC = True

CELERY_REDIRECT_STDOUTS_LEVEL="DEBUG"

LOG_ROOT = normpath(join(CELERY_ROOT, 'log'))
VERSION_FILE = getenv('VERSION_FILE', normpath(join(CELERY_ROOT, 'versions.json')))

DEBUG = getenv("DEBUG", False) == "true"

TERMNEXT_LIB_PATH = getenv("TERMNEXT_LIB_PATH")
CONNECTOR_LIB_PATH = getenv("CONNECTOR_LIB_PATH")
JAVA_LIB_PATH = getenv("JAVA_LIB_PATH")
NER_PATH = getenv("NER_PATH")
NER_MODEL_PATH = getenv("NER_MODEL_PATH")
POS_MODEL_PATH = getenv("POS_MODEL_PATH")
CONFIG_PATH = getenv("CONFIG_PATH")

CELERY_DISABLE_RATE_LIMITS = True if getenv("CELERY_DISABLE_RATE_LIMITS", True) in ["true", True] else False
CELERYD_MAX_TASKS_PER_CHILD = int(getenv("CELERYD_MAX_TASKS_PER_CHILD", 20))
